<?php
/**
 * @file
 * Integrates Moneris Hosted Pay Page payment service.
 *
 * Drupal Core: 7.x
 * Ubercart Core: 3.x
 *
 * This module is developed by myplanetdigital.com
 */

/**
 * Implements hook_menu().
 */
function uc_monerishpp_menu() {
  $items = array();

  $items['cart/monerishpp/complete'] = array(
    'title' => 'Order complete',
    'page callback' => 'uc_monerishpp_complete',
    'access callback' => 'uc_monerishpp_completion_access',
    'type' => MENU_CALLBACK,
  );

  $items['cart/monerishpp/declined'] = array(
    'title' => 'Order declined',
    'page callback' => 'uc_monerishpp_declined',
    'access callback' => 'uc_monerishpp_declined_access',
    'type' => MENU_CALLBACK,
  );

  $items['cart/monerishpp/cancelled'] = array(
    'title' => 'Order cancelled',
    'page callback' => 'uc_monerishpp_cancelled',
    'access callback' => 'uc_monerishpp_cancelled_access',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_init().
 */
function uc_monerishpp_init() {
  global $conf;
  $conf['i18n_variables'][] = 'uc_monerishpp_method_title';
  $conf['i18n_variables'][] = 'uc_monerishpp_checkout_button';
}

/**
 * Allow anyone to complete Moneris orders.
 */
function uc_monerishpp_completion_access() {
  return TRUE;
}

/**
 * Allow anyone to have declined Moneris orders.
 */
function uc_monerishpp_declined_access() {
  return TRUE;
}

/**
 * Allow anyone to have cancelled Moneris orders.
 */
function uc_monerishpp_cancelled_access() {
  return TRUE;
}

/**
 * Implements hook_form_alter().
 */
function uc_monerishpp_form_alter(&$form, &$form_state, $form_id) {

  if ($form_id == 'uc_cart_checkout_review_form' && ($order_id = intval($_SESSION['cart_order'])) > 0) {
    $order = uc_order_load($order_id);
    if ($order->payment_method == 'monerishpp') {
      unset($form['submit']);
      $form['#prefix'] = '<div id="monerishpp-review-table">';
      $form['#suffix'] = '</div>';
      $form = array_merge($form, drupal_get_form('uc_monerishpp_form', $order));
    }
  }
  elseif ($form_id == 'uc_cart_checkout_form') {
    $form['panes']['billing']['billing_phone']['#required'] = TRUE;
  }
}

/**
 * Implements hook_uc_payment_method().
 */
function uc_monerishpp_uc_payment_method() {
  $methods = array();

  $methods[] = array(
    'id' => 'monerishpp',
    'name' => t('Moneris eSelect HPP'),
    'title' => variable_get('uc_monerishpp_method_title', t('Moneris eSelect HPP')),
    'review' => t('Credit card'),
    'desc' => t('Redirect to Moneris eSelect HPP website to pay by credit card.'),
    'callback' => 'uc_payment_method_monerishpp',
    'weight' => 3,
    'checkout' => TRUE,
    'no_gateway' => TRUE,
  );

  return $methods;
}

/**
 * Add settings to the payment method settings form.
 */
function uc_payment_method_monerishpp($op, &$arg1) {
  switch ($op) {
    case 'cart-details':{
      $build = array();

      $build['pay_method'] = array(
        '#type' => 'select',
        '#title' => t('Select your payment type:'),
        '#default_value' => 'CC',
        '#options' => array(
          'CC' => t('Credit card'),
        ),
      );
      unset($_SESSION['pay_method']);

      return $build;
    }
    case 'cart-process':{
      if (isset($_POST['pay_method'])) {
        $_SESSION['pay_method'] = $_POST['pay_method'];
      }
      return;
    }
    case 'settings':{
      $form = array();

      $form['uc_monerishpp_ps_store_id'] = array(
        '#type' => 'textfield',
        '#title' => t('Production PS Store ID'),
        '#default_value' => variable_get('uc_monerishpp_ps_store_id', ''),
        '#description' => t('The PS Store id used for the Moneris eSelect HPP for live transactions.'),
      );

      $form['uc_monerishpp_hpp_key'] = array(
        '#type' => 'textfield',
        '#title' => t('HPP Key'),
        '#default_value' => variable_get('uc_monerishpp_hpp_key', ''),
        '#description' => t('The HPP id used for the Moneris eSelect HPP for live transactions.'),
      );

      $form['uc_monerishpp_test_ps_store_id'] = array(
        '#type' => 'textfield',
        '#title' => t('Test PS Store ID'),
        '#default_value' => variable_get('uc_monerishpp_test_ps_store_id', ''),
        '#description' => t('The PS Store id used for the Moneris eSelect HPP for test transactions.'),
      );

      $form['uc_monerishpp_test_hpp_key'] = array(
        '#type' => 'textfield',
        '#title' => t('Test HPP Key'),
        '#default_value' => variable_get('uc_monerishpp_test_hpp_key', ''),
        '#description' => t('The HPP id used for the Moneris eSelect HPP for test transactions.'),
      );

      $form['uc_monerishpp_transaction_mode'] = array(
        '#type' => 'select',
        '#title' => t('Transaction mode'),
        '#description' => t('Transaction mode used for processing orders.'),
        '#options' => array(
          'production' => t('Production'),
          'test' => t('Test'),
        ),
        '#default_value' => variable_get('uc_monerishpp_transaction_mode', t('Test')),
      );

      $form['uc_monerishpp_method_title'] = array(
        '#type' => 'textfield',
        '#title' => t('Payment method title'),
        '#default_value' => variable_get('uc_monerishpp_method_title', t('Moneris eSelect HPP')),
      );

      $form['uc_monerishpp_order_error_msg'] = array(
        '#type' => 'textfield',
        '#title' => t('Order Error Message'),
        '#description' => t('A message to give to users if there is a problem with the transaction.'),
        '#default_value' => variable_get('uc_monerishpp_order_error_msg', t('An error has occurred during payment. Please contact us to ensure your order has submitted.')),
      );

      if (isset($form['uc_monerishpp_checkout_button'])) {
        unset($form['uc_monerishpp_checkout_button']);
      }

      return $form;
    }
  }
}

/**
 * Form that submits to Moneris HPP.
 */
function uc_monerishpp_form($form, $form_state, $order) {
  // Start building the data to be sent to Moneris.
  $ps_store_id = '';
  $hpp_key = '';
  $mode = variable_get('uc_monerishpp_transaction_mode', '');
  if ($mode == 'production') {
    $ps_store_id = variable_get('uc_monerishpp_ps_store_id', '');
    $hpp_key = variable_get('uc_monerishpp_hpp_key', '');
  }
  else {
    $ps_store_id = variable_get('uc_monerishpp_test_ps_store_id', '');
    $hpp_key = variable_get('uc_monerishpp_test_hpp_key', '');
  }

  // Get taxes out of the order.
  $hst_amount = 0;
  foreach ($order->line_items as $tax_item) {
    if ($tax_item['title'] == 'hst') {
      $hst_amount = $tax_item['amount'];
    }
  }

  // Fill data for the order being sent to Moneris.
  // Let Moneris generate order_id.
  $data = array(
    'ps_store_id' => $ps_store_id,
    'hpp_key' => $hpp_key,
    // Moneris needs to have a "plain" number with two decimal places.
    'charge_total' => number_format($order->order_total, 2, '.', ''),
    // Moneris allows this to act as an invoice #
    'cust_id' => $order->order_id,
    'rvar_order_id' => $order->order_id,
    'hst' => number_format($hst_amount, 2),
    'email' => substr($order->primary_email, 0, 64),
    'bill_first_name' => substr($order->billing_first_name, 0, 128),
    'bill_last_name' => substr($order->billing_last_name, 0, 128),
    'bill_company_name' => substr($order->billing_company, 0, 128),
    'bill_address_one' => substr($order->billing_street1 . ' ' . $order->billing_street2, 0, 128),
    'bill_address_city' => substr($order->billing_city, 0, 128),
    'bill_state_or_province' => $order->billing_zone,
    'bill_postal_code' => $order->billing_postal_code,
    'bill_country'  => $order->billing_country,
    'bill_phone'  => $order->billing_phone,
  );

  // Add all the line items (products) to the order so they can
  // show up in the user's receipt from Moneris.
  $i = 0;
  // The note will contain participant's names up to 50 characters in total.
  $note = '';
  // $order loaded with uc_order_load.
  foreach ($order->products as $product) {
    $i++;
    $data['id' . $i] = $product->model;
    $data['description' . $i] = $product->title;
    $data['quantity' . $i] = $product->qty;
    $data['price' . $i] = number_format($product->price, 2);
    // TODO: this isn't safe if quantity > 1!!!
    $data['subtotal' . $i] = number_format($product->price, 2);
    // Grab full name and only use initials for up to last name.
    $attributes = uc_product_get_attributes($product->nid);
    foreach ($attributes as $attr) {
      if ($attr->name == 'Participant Full Name') {
        $full_name = $product->data['attributes'][(int) $attr->aid];
        break;
      }
    }

    $tokens = explode(' ', $full_name);
    for ($i = 0; $i < sizeof($tokens) - 1; $i++) {
      $note .= substr($tokens[$i], 0, 1) . ' ';
    }
    $note .= $tokens[sizeof($tokens) - 1] . ', ';
  }
  $data['note'] = substr($note, 0, 50);

  $form['#action'] = _monerishpp_post_url();

  foreach ($data as $name => $value) {
    $form[$name] = array('#type' => 'hidden', '#value' => $value);
  }

  return $form;

}

/**
 * Remote form action for moneris.
 */
function _monerishpp_post_url() {
  $mode = variable_get('uc_monerishpp_transaction_mode');
  if ($mode == 'production') {
    return 'https://www3.moneris.com/HPPDP/index.php';
  }
  else {
    // Fail safe to the test environment.
    return 'https://esqa.moneris.com/HPPDP/index.php';
  }
}

/**
 * Callback for completed order.
 */
function uc_monerishpp_complete($cart_id = 0) {
  $output = '';
  $comment = '';
  // Passed through Moneris.
  $order_id = $_POST['rvar_order_id'];
  $order = uc_order_load($order_id);

  if ($order === FALSE || uc_order_status_data($order->order_status, 'state') != 'in_checkout') {
    return variable_get('uc_monerishpp_order_error_msg', '');
  }
  if ((int) $_POST['response_code'] < '50') {
    // Transaction approved.
    $comment = t('Dear !cardholder.  Your order #!order was processed.  Your !type in the amount of !amount was processed on !date at !time.  Authorization #!auth.  Response code !resp. ISO code !iso.  Reference #!refnum.  !message.',
      array(
        '!type' => check_plain($_POST['trans_name']),
        '!order' => check_plain($_POST['rvar_order_id']),
        '!amount' => check_plain($_POST['charge_total']),
        '!date' => check_plain($_POST['date_stamp']),
        '!time' => check_plain($_POST['time_stamp']),
        '!auth' => check_plain($_POST['bank_approval_code']),
        '!resp' => check_plain($_POST['response_code']),
        '!iso' => check_plain($_POST['iso_code']),
        '!message' => check_plain($_POST['message']),
        '!refnum' => check_plain($_POST['bank_transaction_id']),
        '!cardholder' => check_plain($_POST['cardholder']),
      )
    );
    uc_payment_enter($order->order_id, 'monerishpp', $_POST['charge_total'], 0, NULL, $comment);
    $output = uc_cart_complete_sale($order, TRUE);
    uc_cart_empty($cart_id);
  }
  else {
    drupal_set_message(t('Credit card payment declined.'));
    uc_order_comment_save($order->order_id, 0, t('Credit card payment declined at Moneris.'), 'admin');
  }
  $message = $output['#message'];
  $message .= '<br/><br/>' . $comment;
  $output['#message'] = $message;
  return $output;
}

/**
 * Callback for declined credit card payment.
 */
function uc_monerishpp_declined($cart_id = 0) {
  drupal_set_message(t('Credit card payment declined with this message: !message.', array(
    '!message' => check_plain($_POST['message']),
  )), 'error');
  return '';
}

/**
 * Callback for canceled credit card payment.
 */
function uc_monerishpp_cancelled($cart_id = 0) {
  drupal_set_message(t('Credit card payment cancelled at your request.'), 'warning');
  return '';
}
