= Moneris Hosted Pay Page for Ubercart =

This module is an extension to the Ubercart e-commerce system and provides payment processing via the Moneris Hosted Pay Page.

== Original Authors ==

Shanly Suepaul, Imran Khoja and Jonathan Peck.

http://myplanetdigital.com